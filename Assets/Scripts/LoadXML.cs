﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LoadXML : MonoBehaviour
{
    private Team team;
    public int counter = 0;

    public Text teamName_tb;
    public Image kit_imagebox;
    public Text managerName_tb;
    public Text playerName_tb;
    public Text playerNumber_tb;
    public Text playerPosition_tb;

	void Start()
	{
		TextAsset textData = Resources.Load<TextAsset>("roverssquad94"); ;
		ParseFootballTeamXML(textData.text);
        DisplayLoadedData();
	}

    private void ParseFootballTeamXML(string xmlData)
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load(new StringReader(xmlData));

		team = new Team(xmlDoc.FirstChild.Attributes["name"].Value, 
                        xmlDoc.FirstChild.Attributes["manager"].Value,
                        xmlDoc.FirstChild.Attributes["kit"].Value
                       );
		
		string xmlPathPattern = "//team/player";
		XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);

        foreach (XmlNode node in myNodeList)
        {
            string isCaptain = "false";
            if (node.Attributes["captain"] != null)
            {
                isCaptain = node.Attributes["captain"].Value;
            }
            Player player = new Player(node.InnerText,
                                       node.Attributes["number"].Value,
                                       node.Attributes["position"].Value,
                                       isCaptain
                                      );
            team.AddPlayerToTeam(player);
        }

	}

    private void DisplayLoadedData() {
        teamName_tb.text = team.GetTeamName();
        kit_imagebox.sprite = team.GetKitSprite();
        managerName_tb.text = team.GetManagerName();

        DisplayPlayer();
    }

    private void DisplayPlayer() {
        playerName_tb.text = team.GetPlayerFromTeam(counter).GetPlayerName();
        playerNumber_tb.text = team.GetPlayerFromTeam(counter).GetPlayerNumber();
        playerPosition_tb.text = team.GetPlayerFromTeam(counter).GetPlayerPosition().ToString();
    }

    public void updatePosition (int val){
        counter += val;
        if (counter < 0) counter = team.GetFullTeam().Count-1;
        counter = counter % team.GetFullTeam().Count;
        DisplayPlayer();
    }
}
